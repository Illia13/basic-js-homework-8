"use strict";

// let arr = ["travel", "hello", "eat", "ski", "lift"];
// let arrLength = arr.filter((str) => str.length > 3).length;
// console.log(arrLength);

// let arr = [
//   { name: "Іван", age: 25, sex: "чоловіча" },
//   { name: "Марія", age: 30, sex: "жіноча" },
//   { name: "Олександр", age: 22, sex: "чоловіча" },
// ];
// let arrPeople = arr.filter((person) => person.sex === "чоловіча");
// console.log(arrPeople);

function filterBy(array, dataType) {
  return array.filter((item) => typeof item !== dataType);
}

let arr = ["hello", "world", 23, "23", null];
let arrFilter = filterBy(arr, "string");
console.log(arrFilter);
